#+TITLE: Duke Nukem 3D packages for Guix

This is a [[https://www.gnu.org/software/guix/][Guix]] channel dedicated to Duke Nukem 3D, it's community port [[https://eduke32.com/][eduke32]]
and its numerous mods.

A package for the shareware demo is provided: users who do not own the
full version can still enjoy some of the mods.  Note that some mods require the
full version (1.3d or Atomic Edition).

See the [[https://gitlab.com/guix-gaming-channels/games][Guix Gaming Channel]] for more details on installation.
